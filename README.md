# admin public

Documents administratifs de l'ERTIM, ou plus largment liés à l'INALCO.

Attention à ne déposer ici que les documents que nous pouvons diffuser au public, ce dossier étant accessible à toutes les personnes qui disposent du lien, sans nécessité d'avoir un compte Huma-Num.
